package main

import (
	"fmt"
)

//DESC - According to the given input and output, the result is obtained by adding each number to the multiples of 3. Let the initial amount of the variable be 2. 2+0 = (2) the amount of increase in the value. 2+2 = 4 the new value of the variable. 2+3 = (5) the new amount of increase in the value. 4+5 = 9 the new value of the variable.

func main() {
	createNumberSeries(9)
}

// * Default pattern is 2,2,3
var firstNumber = 2
var secondNumber = 2
var thirdNumber = 3

func createNumberSeries(count int) {
	if firstNumber > count {
		return
	}
	//DESC - Print the value of the variable
	fmt.Println(firstNumber)
	//DESC - The value of the variable is increased by the amount of increase in the value.
	firstNumber, secondNumber, thirdNumber = firstNumber+secondNumber, secondNumber+thirdNumber, thirdNumber+3
	//DESC - Recursive function is called.
	createNumberSeries(count)
}

//DESC - Input 9
//DESC - Output 2 4 9
